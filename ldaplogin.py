# coding: utf8

from __future__ import absolute_import, division, print_function
from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)

import ldap
import os
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="Add verbosity", action="store_true")
parser.add_argument("-u", "--user", help="Unix name", required=True)
parser.add_argument("-p", "--password", help="Password",required=True)
args = parser.parse_args()


if args.verbose:
    print( "[Verbosity mode]" )
print( os.getcwd() )
conn = ldap.initialize('ldap://<something>')


f_ldap = False
try:
    f_ldap = conn.simple_bind_s( args.user+"@<something>", args.password)
except:
    print( "login error" )
else:
    print( "login succesfully!" )


if f_ldap:
    pass

exit(0)

