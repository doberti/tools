#!/bin/bash

# Autor: Daniel Oberti
revision="2020-01-27"
path_lics = '<something>'
### START DEFAULTS ###
OPTION="check_logs"
LICENSENAME=""
FEATURE=""
AUXBUSCAR=""
DIASATRAS=0
VERBOSE=0
SHOWDATE=0
LOGFILE=<path_logs>/*/*.log
THEUSER=$USER
FECHA=`date +"%Y%m%d_%H%M"`
### END DEFAULTS ###

function showhelp()
{
	echo "######### LICENSE DEBUG #########"
	echo "MODO DE USO:"
	echo "<this> [ARGUMENTOS]"
	echo
	echo "ARGUMENTOS:"
        echo "          -o/--option: Indicar la operacion que se desea realizar (Default: 'check_logs')."
        echo "                       Valores: get_connection|check_logs|get_licname|get_expiration_warning"
        echo
        echo "          --lic: Indicar el nombre de la licencia."
        echo
        echo "          -f|--feature: Indicar el nombre de la feature (in some options it isn't case sensitive but in others could be)."
        echo
	echo "		--version: Indica la revision a la que pertenece el comando."
	echo
        echo "          v/--verbose: Modo debug"
        echo
        echo "          --showdate: Mostrar fechas dentro de los logs"
        echo
        echo "          -d/--dia <DIA>: Indica a que dia va a buscar los logs. Por defecto es 0 para hoy, 1 para ayer, etc"
        echo
	echo "		-u/--user <USER>: Para indicar un user diferente al actual."
	echo
	echo "###################################"
	exit 0
}

ARGS=$(getopt -q -o "o:u:d:f: vh" -l "option:,user:,dia:,feature:,lic:,verbose,help,version,showdate" -n "argumentos" -- "$@");
eval set -- "$ARGS";

while [ $# -gt 0 ]; do
  case "$1" in
    --version)
      echo "Version: $revision"
      ;;
    -v|--verbose)
      VERBOSE=1
      ;;
    --showdate)
      SHOWDATE=1
      ;;
    -u|--user)
      THEUSER="$2"
      ;;
    -l|--log)
      LOGFILE="$2"
      shift;
      ;;
    -d|--dia)
      DIASATRAS="$2"
      shift;
      ;;
    --lic)
      LICENSENAME="$2"
      shift;
      ;;
    -f|--feature)
      FEATURE="$2"
      shift;
      ;;
    -o|--option)
      OPTION="$2"
      shift;
      ;;
    -h|--help)
      showhelp
      shift;
      ;;
    --)
      shift;
      break;
      ;;
  esac
  shift
done

echo "OPTION: $OPTION"
MIDATE=`date --date="-$DIASATRAS day" +%m/%d/%Y`

if [ "$SHOWDATE" == "1" ]; then
    AUXBUSCAR="|TIMESTAMP"
fi


#### START get_licname ####
if [ "$OPTION" == "get_licname" ]; then
    ### START CHECKS ###
    if [ "$FEATURE" == "" ]; then
        echo "You must indicate a feature name (-f|--feature <FEATURE>)"
        echo "Please try again"
        exit 1
    fi
    ### END CHECKS ###
    fgrep -i -H -r "$FEATURE" path_lics
fi
#### END get_licname ####

#### START get_connection ####
if [ "$OPTION" == "get_connection" ]; then
    ### START CHECKS ###
    if [ "$LICENSENAME" == "" ]; then
        echo "You must indicate a license name (--lic <LICENSENAME>)"
        echo "Please try again"
        exit 1
    fi
    ### END CHECKS ###
    echo "LICENSENAME: $LICENSENAME"
    fgrep -i -H -r "SERVER " path_lics | grep .dat | cut -d " " -f 1,4,2 | grep $LICENSENAME
fi
#### END get_connection ####




#### START check_logs ####
if [ "$OPTION" == "check_logs" ]; then

    ### START CHECKS ###
    if [ "<...>" == "" ]; then
        echo "You must indicate a <...> (-...|--... <...>)"
        echo "Please try again"
        exit 1
    fi
    ### END CHECKS ###


echo "#### checking logs... ####"
COMMAND="grep -E \"TIMESTAMP $MIDATE\" -A 100000000 -H $LOGFILE | grep -E \"ERROR|DENIED|WARNING$AUXBUSCAR\" | egrep -E \"$THEUSER$AUXBUSCAR\""

if [ "$VERBOSE" == "1" ]; then
    echo "user: $THEUSER"
    echo "fecha: $FECHA"
    echo "DIASATRAS: $DIASATRAS"
    echo "SHOWDATE: $SHOWDATE"
    echo "COMMAND: $COMMAND"
fi
grep -E "TIMESTAMP $MIDATE" -A 100000000 -H $LOGFILE | grep -E "ERROR|DENIED|WARNING$AUXBUSCAR" | grep -E "$THEUSER$AUXBUSCAR"
if [ "$VERBOSE" == "1" ]; then echo "$COMMAND"; fi

fi
#### END check_logs ####

#### START get_expiration_warning ####
if [ "$OPTION" == "get_expiration_warning" ]; then
    ### START CHECKS ###
    if [ "<...>" == "" ]; then
        echo "You must indicate a <...> (-...|--... <...>)"
        echo "Please try again"
        exit 1
    fi
    ### END CHECKS ###

    #COMMAND="grep -E \"TIMESTAMP $MIDATE\" -A 100000000 -H $LOGFILE | grep -E \"EXPIRATION WARNING$AUXBUSCAR\" "
    COMMAND="grep -E \"EXPIRATION\" -A 100000000 -H  $LOGFILE | grep \"$MIDATE\""
    grep -E "EXPIRATION" -A 100000000 -H $LOGFILE | grep "$MIDATE"
    if [ "$VERBOSE" == "1" ]; then echo "$COMMAND"; fi
fi
#### END get_expiration_warning ####

exit 0
