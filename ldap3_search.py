# coding: utf8
# python -m pip install --upgrade pip
# pip install python-ldap
# Microsoft Visual C++ 14.0 is required. Get it with "Build Tools for Visual Studio": https://visualstudio.microsoft.com/downloads/
# I added the installer of build tools for visual studio in \\las-cifs01\software\vs_buildtools__770000337.1593636432.exe
# Restart your computer after installation
# pip install ldap3
import ldap3
from pprint import pprint

check_user = 'doberti'
LDAP_LOGIN = '<something>'
LDAP_PASSWORD = '<something>'
SERVER_URI = 'ldap://<something>:<something>' #'ldap://ldap.example.com'
SEARCH_BASE = 'dc=<something>,dc=local' #'OU=Employees,OU=<something>,DC=local' #'ou=users,dc=example,dc=com'
SEARCH_FILTER = '(sAMAccountName='+ check_user +')' #'(uid=rob)'
#ATTRS = ['*']
ATTRS = ['Manager']

server = ldap3.Server(SERVER_URI)
with ldap3.Connection(server,  LDAP_LOGIN, LDAP_PASSWORD, auto_bind=True) as conn:
    conn.search(SEARCH_BASE, SEARCH_FILTER, attributes=ATTRS)
    #print(conn.entries)
    aux_cn = str(conn.entries[0]['manager']).split(',')[0]
    print( aux_cn )

SEARCH_FILTER = '(' + aux_cn + ')'
ATTRS = ['sAMAccountName']
#ATTRS = ['*']
server = ldap3.Server(SERVER_URI)
with ldap3.Connection(server,  LDAP_LOGIN, LDAP_PASSWORD, auto_bind=True) as conn:
    conn.search(SEARCH_BASE, SEARCH_FILTER, attributes=ATTRS)
    #pprint(conn.entries)
    for entrada in conn.entries:
        print( entrada['sAMAccountName'] )





