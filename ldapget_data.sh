#!/bin/bash

QUERYSTRING=${1?Error: no username given}

############## LDAP-CONFIG ################

HOSTNAME='<something>'
PORT='<something>'
USERNAME='<something>'
PASSWORD='<something>'
SEARCHBASE='dc=<something>,dc=local'

###########################################

USERDETAIL=`ldapsearch -x -LLL -h ${HOSTNAME} -p $PORT -D ${USERNAME} -w ${PASSWORD} -b "${SEARCHBASE}" -s sub "sAMAccountName=${QUERYSTRING}" cn l manager title mail telephoneNumber`

echo "$USERDETAIL"
ldapsearch -x -LLL -h ${HOSTNAME} -p $PORT -D ${USERNAME} -w ${PASSWORD} -b "${SEARCHBASE}" -s sub "sAMAccountName=${QUERYSTRING}"

FULLNAME=`echo $USERDETAIL | grep -oP '(?<=cn:).*(?= l:)'`
LOCATION=`echo $USERDETAIL | grep -oP '(?<=l:).*(?=title:)'`
TITLE=`echo $USERDETAIL | grep -oP '(?<=title:).*(?=telephoneNumber:)'`
TELEPHONE=`echo $USERDETAIL | grep -oP '(?<=telephoneNumber:).*(?=mail:)'`
MAIL=`echo $USERDETAIL | grep -oP '(?<=mail:).*(?=manager:)'`
MANAGER=`echo $USERDETAIL | grep -oP '(?<=manager: CN=).*(?=,OU)'`
MANAGER=`echo "${MANAGER%%,*}"`


echo "cn=${MANAGER}"
MANAGERDETAIL=`ldapsearch -x -LLL -h ${HOSTNAME} -p $PORT -D ${USERNAME} -w ${PASSWORD} -b "${SEARCHBASE}" -s sub "cn=${MANAGER}" cn l manager title mail telephoneNumber`

MANAGERFULLNAME=`echo $MANAGERDETAIL | grep -oP '(?<=cn:).*(?= l:)'`
MANAGERLOCATION=`echo $MANAGERDETAIL | grep -oP '(?<=l:).*(?=title:)'`
MANAGERTITLE=`echo $MANAGERDETAIL | grep -oP '(?<=title:).*(?=telephoneNumber:)'`
MANAGERTELEPHONE=`echo $MANAGERDETAIL | grep -oP '(?<=telephoneNumber:).*(?=mail:)'`
MANAGERMAIL=`echo $MANAGERDETAIL | grep -oP '(?<=mail:).*(?=manager:)'`


############## OUTPUT #####################

echo ""
echo "############### USER-INFO ##################"
echo "Full name:${FULLNAME}"
echo "Location:${LOCATION}"
echo "Title:${TITLE}"
echo "Telephone:${TELEPHONE}"
echo "mail:${MAIL}"

echo ""
echo "############### MANAGER-INFO ###############"
echo "Full name:${MANAGERFULLNAME}"
echo "Location: ${MANAGERLOCATION}"
echo "Title: ${MANAGERTITLE}"
echo "Telephone:${MANAGERTELEPHONE}"
echo "mail:${MANAGERMAIL}"
echo ""

###########################################
