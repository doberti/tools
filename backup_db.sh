#!/bin/bash                                          
#yum -y install zip
 
#restonring db:
#unzip [file.zip]
#mysql -u [username] -p [password] < backupFile.sql


# CREATION of the remote mysql user backupin
#   CREATE USER 'backupin'@'%' IDENTIFIED BY '<PASSWORD>';
#   GRANT SELECT,SHOW DATABASES,LOCK TABLES On *.* To 'backupin'@'%';
#   FLUSH PRIVILEGES;


PROJECT=$1
RUTA=$2
myuser=$3
mypass=$4
host=$5

cd $RUTA

date +"%Y%m%d_%H%M" >> ${PROJECT}_dbbackup.log
 
_FECHA=`date +"%Y-%m-%d_%H-%M"`
 
args="-u"$myuser" -p"$mypass" -h"$host"  --add-drop-database --add-locks --create-options --complete-insert --comments --disable-keys --dump-date --extended-insert --quick --routines --triggers" 
 
mysql -u$myuser -p"$mypass" -h "$host" -e 'show databases' | grep -Ev "(Database|information_schema)" > ${PROJECT}_databases.list
 
echo "Se volcarán las siguientes bases de datos:"
mysql -u$myuser -p"$mypass" -h "$host" -e 'select table_schema "DATABASE",convert(sum(data_length+index_length)/1048576, decimal(6,2)) "SIZE (MB)" from information_schema.tables where table_schema!="information_schema" group by table_schema;'
CONT=1
while [ $CONT -eq 1 ]
do
        K='S'
        #COMENTADO: ESTA PARTE ES PARA PREGUNTAR SI SE DESEA EJECUTAR
        #echo -n "¿Desea continuar? (S/N): "
        #read -n 1 K
        [[ "$K" == "N" || "$K" == "n" ]] && { echo ""; exit 0; }
        [[ "$K" == "S" || "$K" == "s" ]] && { CONT=0; }
        echo ""
done
 
while read DB
do
        dump=${PROJECT}"dump_"$DB".sql"
        echo -n $dump"... "
        echo "/usr/bin/mysqldump -B $args $DB > $dump"
        /usr/bin/mysqldump -B ${args} $DB > $dump
        echo "OK."
done < ${PROJECT}_databases.list
 
rm -f ${PROJECT}_databases.list
 
#rm -f *sqlbk.zip
find $0 -name "*sqlbk.zip" -mtime +20 -exec rm -f {} \;
#find $0 -name "*_sqlbk.zip" -exec rm {} \;
 
#TODO EN ARCHIVOS ZIP SEPARADOS
ls | grep ".sql$" | awk '{system("zip -rm \""$0"\"_$(date +\"%Y%m%d_%H%M\")_sqlbk.zip \""$0"\"")}'
 
#TODO EN UN ARCHIVO ZIP
#zip -rm ${_FECHA}_sqlbk *.sql


date +"%Y%m%d_%H%M" >> ${PROJECT}_dbbackup.log
 
exit 0
