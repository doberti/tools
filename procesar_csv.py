# -*- coding: utf8 -*-
# doberti - version 0.1

# procesar_csv.py  
import pylab as pl  
import csv
import operator
import commands
import time
import sys

sys.path.append('/home/doberti/')
from libs import *
#

#/*******************************/
ruta = '<csv path>'
user = 'doberti'
tiempo_dormir = 60
ciclos_max = 99999999999999999999999999999999999
#/*******************************/

csvfile = 'medicion.csv'
identificador = ""

print("1: Monitorear")
print("2: Graficar")
print("3: Monitorear y Graficar")
opcion = int(raw_input())

lista_data = []
if opcion==1 or opcion==3:
	print("Introduzca un identificador de esta captura (sin espacios):")
	identificador = raw_input()
	if identificador!="": csvfile = identificador+csvfile
	#csvsalida = open(csvfile, 'w', newline='')
	csvsalida = open(csvfile, 'w')
	salida = csv.writer(csvsalida)
	print("Introduzca cuantos ciclos de monitoreo maximo desea (por defecto es "+str(ciclos_max)+" y debera usar un CTRL + C)")
	aux = raw_input()
	if aux!='': ciclos_max = int(aux)
	
	ciclos = 0
	time_inicial = int(time.time())
	while True:
		ciclos += 1
		if ciclos > ciclos_max: break
		ts = str(int(time.time()) - time_inicial)
                print "FECHA: "+str(time.strftime('%Y-%m-%d %H:%M:%S'))
		print ("ts:"+str(ts))
		cjobs = str(get_cantidad_jobs(user,cluster='NC'))
		print ("cjobs:"+str(cjobs))
                print ("ruta:"+str(ruta))
                size = '0'
		size = str(spaceinMB(get_size(ruta)))  # para discos rapidos
                #size = str(spaceinTB( commands.getoutput("df -h <path> |  tail -n1 | awk '{print $2}'") )) #para discos lentos
		print ("size:"+str(size))
		salida.writerow([ts,cjobs,size])
		dormir(tiempo_dormir)
	
	del salida
	csvsalida.close()

if opcion==2 or opcion==3:
	pngname1 = ''
	pngname2 = ''
	if opcion==2:
		print( commands.getoutput('ls -lh *medicion.csv') )
		print("Introduzca nombre del archivo csv que desea graficar:")
		csvfile = raw_input()
		pngname1 = csvfile+"imagen1.png"
                pngname2 = csvfile+"imagen2.png"
	else:
		pngname1 = identificador+"imagen1.png"
		pngname2 = identificador+"imagen2.png"
	graficar_csv(csvfile,'Time[seconds]','jobs', pngname1)
	graficar_csv(csvfile,'Time[seconds]','DiskSpace', pngname2, col1=0, col2=2)

exit(0)
