# Compilar y ejecutar:
# javac SimpleLdapAuthentication.java
# java SimpleLdapAuthentication

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;


import java.util.Enumeration;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;


public class SimpleLdapAuthentication
{
	public static void main(String[] args)
	{


String userName = "doberti";
String userPassword = "<something>";


String url = "ldap://<something>:<something>";
//Hashtable env = new Hashtable();
                Hashtable<String, String> env =
                        new Hashtable<String, String>();
env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
env.put(Context.PROVIDER_URL, url);
env.put(Context.SECURITY_AUTHENTICATION, "simple");
env.put(Context.SECURITY_PRINCIPAL, "CN=ldap User,CN=Users,DC=<something>,DC=local");
env.put(Context.SECURITY_CREDENTIALS, "<something>");

DirContext ctx; 
try {
    ctx = new InitialDirContext(env);
    System.out.println("connected");
    System.out.println(ctx.getEnvironment());
    // do something useful with the context...
    System.out.println("Logged");
    //////ctx.close();


       NamingEnumeration<SearchResult> results = null;

       //try {
          SearchControls controls = new SearchControls();
          controls.setSearchScope(SearchControls.SUBTREE_SCOPE); // Search Entire Subtree
          controls.setCountLimit(1);   //Sets the maximum number of entries to be returned as a result of the search
          controls.setTimeLimit(5000); // Sets the time limit of these SearchControls in milliseconds

          String searchString = "(&(objectCategory=user)(sAMAccountName=" + userName + "))";

          results = ctx.search("", searchString, controls);

          if (results.hasMore()) {

              SearchResult result = (SearchResult) results.next();
              Attributes attrs = result.getAttributes();
              Attribute dnAttr = attrs.get("distinguishedName");
              String dn = (String) dnAttr.get();

              // User Exists, Validate the Password
              env.put(Context.SECURITY_PRINCIPAL, dn);
              env.put(Context.SECURITY_CREDENTIALS, userPassword);

              new InitialDirContext(env); // Exception will be thrown on Invalid case
              System.out.println("user login");
          } 
          else 
              System.out.println("login user wrong");

       } catch (AuthenticationException e) { // Invalid Login
              System.out.println("exception");
     
       } catch (NamingException e) {
          throw new RuntimeException(e);
       } 
}

}

