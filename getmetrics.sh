#!/bin/bash

     
FECHA=`date +"%Y-%m-%d_%H:%M"`
 
echo $FECHA
echo "______________________________________________"
echo "_______  CARACTERISTICAS DEL SERVIDOR  _______"
echo "______________________________________________"
ifconfig eth0 | grep 'inet addr' | awk {'print $2'}
echo "******** TARJETA DE RED ********"
ethtool eth0 | grep Speed
echo "************ CPU ***************"
cat /proc/cpuinfo | grep "model name"
cat /proc/cpuinfo | grep "cache size"
cat /proc/cpuinfo | grep "address sizes"
echo "************ RAM ***************"
dmidecode --type memory | grep -A50 "Memory Device" | grep 'Size'
dmidecode --type memory | grep -A50 "Memory Device" | grep 'Type:'
dmidecode --type memory | grep -A50 "Memory Device" | grep 'Speed:'
echo "************* HD ****************"
lshw -C disk | grep description
lshw -C disk | grep size
echo "************ KERNEL *************"
cat /proc/version
echo "********* ARQUITECTURA **********"
uname -m
echo "______________________________________________"
 
DESCANSO=10
MAX_ITERACIONES=10000
CONTADOR=0
LIMITE=100000

MYHORA=$(date +"%H";)
NODO_IP=$(ifconfig | grep eth0 -A1 | grep 'inet addr' | awk '{print $2}')
CLIENTE="${HOSTNAME}_(${NODO_IP})"
RUTA='/home/doberti/'
CSV="MEDICION"
echo "" > ${RUTA}${CSV}_${MYHORA}.csv
 
echo "Iniciando analisis..."
 
TITULOS="TIEMPO;df -h;du -sh; nc jobs"
echo "$TITULOS" >> ${RUTA}${CSV}_${MYHORA}.csv
 
MYHORA=$(date +"%H";)
 
echo "$TITULOS" >> ${RUTA}${CSV}_${MYHORA}.csv
while [ $CONTADOR -le $MAX_ITERACIONES ]; do
    sleep $DESCANSO
    
    data1=`<comando para obtener dato>`
    data2=`<comando para obtener dato>`
    data3=`<comando para obtener dato>`
    
    FECHA=`date +"%Y-%m-%d_%H:%M"`
    MYHORA=`date +"%H"`
    HORA=`date +"%H:%M:%S"`
    MEDICIONES="$HORA;$data1;$data2;$data3"
     
    #para hacer un split usar (| awk -F"." '{print $1}' | awk -F"," '{print $1}')
    echo "$MEDICIONES" >> ${RUTA}${CSV}_${MYHORA}.csv
    echo "$MEDICIONES"
 
 
    #*** monitorear procesos que mas consumen ancho de banda ***
    #nethogs eth0
     
    #*** monitorear ancho de banda ***
    #bmon
    #ethstatus
     
    tamanio=$(stat -c %s  ${CSV}_${MYHORA}.csv)
    if [ $tamanio -gt $LIMITE ]; then
        echo "$TITULOS"
        #echo "" > ${RUTA}${CSV}_${MYHORA}.csv
        #echo "$TITULOS"
        #echo "$TITULOS" >> ${RUTA}${CSV}_${MYHORA}.csv
    fi
    #clear
    #tail -n17 $1
 
done
 
echo "Estadisticas finalizadas!"
 
exit 0


#> echo "" > getmetrics.sh; chmod +x getmetrics.sh; vim getmetrics.sh

